package com.realalknowles.host;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.realalknowles.model.Model;
import com.realalknowles.remote.Remote;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class HostTest {

    @LocalServerPort
    private int localServerPort;

    @Test
    public void getModel_success() throws UnirestException {
        Remote remote = new Remote("http://localhost:" + localServerPort);
        Model model = remote.getModel();
        Assert.assertEquals("I am the model", model.getContent());
    }

}
