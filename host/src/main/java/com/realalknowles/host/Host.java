package com.realalknowles.host;

import com.realalknowles.model.Model;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class Host {

    public static void main(final String[] args) {
        SpringApplication.run(Host.class, args);
    }

    @GetMapping("/model")
    public Model getModel() {
        Model model = new Model();
        model.setContent("I am the model");
        return model;
    }

}
