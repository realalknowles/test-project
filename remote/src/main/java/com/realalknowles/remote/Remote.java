package com.realalknowles.remote;

import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.realalknowles.model.Model;
import io.vavr.control.Try;

public class Remote {
    static {
        Unirest.setObjectMapper(new ObjectMapper() {

            com.fasterxml.jackson.databind.ObjectMapper jacksonMapper
                = new com.fasterxml.jackson.databind.ObjectMapper();

            public String writeValue(Object value) {
                return Try.of(() ->
                    jacksonMapper.writeValueAsString(value))
                    .getOrElseThrow(() -> new Error("Could not serialize data"));
            }

            public <T> T readValue(String value, Class<T> valueType) {
                return Try.of(() ->
                    jacksonMapper.readValue(value, valueType))
                    .getOrElseThrow(() -> new Error("Could not deserialize data"));
            }
        });
    }

    private final String path;

    public Remote(String path) {
        this.path = path;
    }

    @SuppressWarnings("unused")
    public Model getModel() throws UnirestException {
        return Unirest.get(path + "/model")
            .asObject(Model.class)
            .getBody();
    }

}
