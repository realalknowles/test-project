package com.realalknowles.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SuppressWarnings("unused")
public class Model {

    private String content;

}
